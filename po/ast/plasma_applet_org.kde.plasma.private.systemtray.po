# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-20 01:39+0000\n"
"PO-Revision-Date: 2023-05-09 22:46+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr ""

#: package/contents/config/config.qml:18
#, kde-format
msgid "Entries"
msgstr "Entraes"

#: package/contents/ui/ConfigEntries.qml:34
#, kde-format
msgid "Application Status"
msgstr ""

#: package/contents/ui/ConfigEntries.qml:36
#, kde-format
msgid "Communications"
msgstr ""

#: package/contents/ui/ConfigEntries.qml:38
#, kde-format
msgid "System Services"
msgstr "Servicios del sistema"

#: package/contents/ui/ConfigEntries.qml:40
#, kde-format
msgid "Hardware Control"
msgstr "Control de hardware"

#: package/contents/ui/ConfigEntries.qml:43
#, kde-format
msgid "Miscellaneous"
msgstr ""

#: package/contents/ui/ConfigEntries.qml:90
#, kde-format
msgctxt "Name of the system tray entry"
msgid "Entry"
msgstr ""

#: package/contents/ui/ConfigEntries.qml:95
#, kde-format
msgid "Visibility"
msgstr "Visibilidá"

#: package/contents/ui/ConfigEntries.qml:101
#, kde-format
msgid "Keyboard Shortcut"
msgstr ""

#: package/contents/ui/ConfigEntries.qml:248
#, kde-format
msgid "Shown when relevant"
msgstr ""

#: package/contents/ui/ConfigEntries.qml:249
#, kde-format
msgid "Always shown"
msgstr ""

#: package/contents/ui/ConfigEntries.qml:250
#, kde-format
msgid "Always hidden"
msgstr ""

#: package/contents/ui/ConfigEntries.qml:251
#, kde-format
msgid "Disabled"
msgstr ""

#: package/contents/ui/ConfigEntries.qml:316
#, kde-format
msgid "Always show all entries"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:23
#, kde-format
msgctxt "The arrangement of system tray icons in the Panel"
msgid "Panel icon size:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:25
#, kde-format
msgid "Small"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:32
#, kde-format
msgid "Scale with Panel height"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:33
#, kde-format
msgid "Scale with Panel width"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:39
#, kde-format
msgid "Automatically enabled when in Touch Mode"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:48
#, kde-format
msgctxt "@label:listbox The spacing between system tray icons in the Panel"
msgid "Panel icon spacing:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:51
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Small"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:55
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Normal"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:59
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Large"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:82
#, kde-format
msgctxt "@info:usagetip under a combobox when Touch Mode is on"
msgid "Automatically set to Large when in Touch Mode"
msgstr ""

#: package/contents/ui/ExpandedRepresentation.qml:62
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr ""

#: package/contents/ui/ExpandedRepresentation.qml:74
#, kde-format
msgid "Status and Notifications"
msgstr ""

#: package/contents/ui/ExpandedRepresentation.qml:140
#, kde-format
msgid "More actions"
msgstr "Más aiciones"

#: package/contents/ui/ExpandedRepresentation.qml:234
#, kde-format
msgid "Keep Open"
msgstr ""

#: package/contents/ui/ExpanderArrow.qml:24
#, kde-format
msgid "Expand System Tray"
msgstr ""

#: package/contents/ui/ExpanderArrow.qml:25
#, kde-format
msgid "Show all the items in the system tray in a popup"
msgstr ""

#: package/contents/ui/ExpanderArrow.qml:38
#, kde-format
msgid "Close popup"
msgstr ""

#: package/contents/ui/ExpanderArrow.qml:38
#, kde-format
msgid "Show hidden icons"
msgstr ""
