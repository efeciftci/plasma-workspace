# translation of plasma_applet_org.kde.plasma.icon.pot to Esperanto
# Copyright (C) 2016 Free Software Foundation, Inc.
# This file is distributed under the same license as the plasma-workspace package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-25 01:41+0000\n"
"PO-Revision-Date: 2023-04-07 23:35+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: iconapplet.cpp:93
#, kde-format
msgid "Failed to create icon widgets folder '%1'"
msgstr "Malsukcesis krei piktograman dosierujon '%1'"

#: iconapplet.cpp:139
#, kde-format
msgid "Failed to copy icon widget desktop file from '%1' to '%2'"
msgstr ""
"Malsukcesis kopii piktogramfenestraĵan labortablodosieron de '%1' al '%2'"

#: iconapplet.cpp:351
#, kde-format
msgid "Open Containing Folder"
msgstr "Malfermi Enhavan Dosierujon"

#: iconapplet.cpp:566
#, kde-format
msgid "Properties for %1"
msgstr "Proprecoj for %1"

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Properties"
msgstr "Proprecoj"
