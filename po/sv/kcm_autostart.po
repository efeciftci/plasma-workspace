# translation of kcm_autostart.po to Swedish
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2008, 2010, 2012, 2015, 2017, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-27 01:41+0000\n"
"PO-Revision-Date: 2022-08-03 09:30+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.08.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: autostartmodel.cpp:374
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr "\"%1\" är inte en absolut webbadress."

#: autostartmodel.cpp:377
#, kde-format
msgid "\"%1\" does not exist."
msgstr "\"%1\" finns inte."

#: autostartmodel.cpp:380
#, kde-format
msgid "\"%1\" is not a file."
msgstr "\"%1\" är inte en fil."

#: autostartmodel.cpp:383
#, kde-format
msgid "\"%1\" is not readable."
msgstr "\"%1\" är inte läsbar."

#: ui/entry.qml:52
#, fuzzy, kde-format
#| msgid "Name"
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr "Namn"

#: ui/entry.qml:57
#, fuzzy, kde-format
#| msgid "Status"
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr "Status"

#: ui/entry.qml:62
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr ""

#: ui/entry.qml:71
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr ""

#: ui/entry.qml:71
#, fuzzy, kde-format
#| msgid "Startup"
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr "Start"

#: ui/entry.qml:105
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr ""

#: ui/entry.qml:109
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr ""

#: ui/main.qml:33
#, kde-format
msgid "Make Executable"
msgstr "Gör körbart"

#: ui/main.qml:53
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr "Filen '%1' måste vara körbar för att köra vid utloggning."

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr "Filen '%1' måste vara körbar för att köra vid inloggning."

#: ui/main.qml:66
#, fuzzy, kde-format
#| msgid "Add…"
msgctxt "@action:button"
msgid "Add…"
msgstr "Lägg till…"

#: ui/main.qml:69
#, fuzzy, kde-format
#| msgid "Add Application…"
msgctxt "@action:button"
msgid "Add Application…"
msgstr "Lägg till program…"

#: ui/main.qml:74
#, fuzzy, kde-format
#| msgid "Add Login Script…"
msgctxt "@action:button"
msgid "Add Login Script…"
msgstr "Lägg till inloggningsskript…"

#: ui/main.qml:79
#, fuzzy, kde-format
#| msgid "Add Logout Script…"
msgctxt "@action:button"
msgid "Add Logout Script…"
msgstr "Lägg till utloggningsskript…"

#: ui/main.qml:119
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""

#: ui/main.qml:143
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr ""

#: ui/main.qml:148 unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr ""

#: ui/main.qml:154
#, fuzzy, kde-format
#| msgid "Properties"
msgctxt "@action:button"
msgid "See properties"
msgstr "Egenskaper"

#: ui/main.qml:160
#, fuzzy, kde-format
#| msgid "Remove"
msgctxt "@action:button"
msgid "Remove entry"
msgstr "Ta bort"

#: ui/main.qml:170
#, kde-format
msgid "Applications"
msgstr "Program"

#: ui/main.qml:173
#, kde-format
msgid "Login Scripts"
msgstr "Inloggningsskript"

#: ui/main.qml:176
#, kde-format
msgid "Pre-startup Scripts"
msgstr "Skript innan start"

#: ui/main.qml:179
#, kde-format
msgid "Logout Scripts"
msgstr "Utloggningsskript"

#: ui/main.qml:187
#, kde-format
msgid "No user-specified autostart items"
msgstr "Inget användarspecificerade objekt för automatisk start"

#: ui/main.qml:188
#, fuzzy, kde-kuit-format
#| msgctxt "@info"
#| msgid "Click the <interface>Add…</interface> button below to add some"
msgctxt "@info 'some' refers to autostart items"
msgid "Click the <interface>Add…</interface> button to add some"
msgstr ""
"Klicka på knappen <interface>Lägg till…</interface> nedan för att lägga till "
"några"

#: ui/main.qml:203
#, kde-format
msgid "Choose Login Script"
msgstr "Välj inloggningsskript"

#: ui/main.qml:223
#, kde-format
msgid "Choose Logout Script"
msgstr "Välj utloggningsskript"

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr ""

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr ""

#: unit.cpp:28
#, fuzzy, kde-format
#| msgid "Startup"
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr "Start"

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr ""

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr ""

#: unit.cpp:153
#, kde-format
msgid "Failed to open journal"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Stefan Asserhäll"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "stefan.asserhall@bredband.net"

#~ msgid "Autostart"
#~ msgstr "Automatisk start"

#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Inställningsmodul för hantering av automatisk sessionsstart"

#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "Copyright © 2006-2020 Hantering av automatisk start-gruppen"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Utvecklare"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Add..."
#~ msgstr "Lägg till..."

#~ msgid "Shell script path:"
#~ msgstr "Sökväg till skalskript:"

#~ msgid "Create as symlink"
#~ msgstr "Skapa som symbolisk länk"

#~ msgid "Autostart only in Plasma"
#~ msgstr "Starta bara automatiskt i Plasma"

#~ msgid "Command"
#~ msgstr "Kommando"

#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Kör vid"

#~ msgid "Session Autostart Manager"
#~ msgstr "Hantering av automatisk sessionsstart"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "Aktiverat"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Inaktiverat"

#~ msgid "Desktop File"
#~ msgstr "Skrivbordsfil"

#~ msgid "Script File"
#~ msgstr "Skriptfil"

#~ msgid "Add Program..."
#~ msgstr "Lägg till program..."

#~ msgid "Before session startup"
#~ msgstr "Innan sessionsstart"

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr ""
#~ "Bara filer med filändelsen \".sh\" är tillåtna för att ställa in miljön."

#~ msgid "Pre-KDE startup"
#~ msgstr "Innan KDE har startat"

#~ msgid "Shutdown"
#~ msgstr "Avstängning"
